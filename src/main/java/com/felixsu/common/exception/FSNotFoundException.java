package com.felixsu.common.exception;

/**
 * Created on 11/14/16.
 *
 * @author felixsoewito
 */
public class FSNotFoundException extends Exception {

    public FSNotFoundException(String message) {
        super(message);
    }

    public FSNotFoundException(Throwable cause) {
        super(cause);
    }

    public FSNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
