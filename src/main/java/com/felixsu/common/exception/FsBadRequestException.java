package com.felixsu.common.exception;

/**
 * Created on 11/14/16.
 *
 * @author felixsoewito
 */
public class FsBadRequestException extends Exception {

    public FsBadRequestException(String message) {
        super(message);
    }

    public FsBadRequestException(Throwable cause) {
        super(cause);
    }

    public FsBadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
