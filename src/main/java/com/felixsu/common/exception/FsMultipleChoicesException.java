package com.felixsu.common.exception;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public class FsMultipleChoicesException extends Exception {

    public FsMultipleChoicesException(String message) {
        super(message);
    }

    public FsMultipleChoicesException(String message, Throwable cause) {
        super(message, cause);
    }

    public FsMultipleChoicesException(Throwable cause) {
        super(cause);
    }
}
