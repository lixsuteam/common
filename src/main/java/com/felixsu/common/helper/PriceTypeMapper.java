package com.felixsu.common.helper;

import com.felixsu.common.model.cinema.CinemaPrice;

/**
 * Created by felixsu on 16/04/2017.
 */
public class PriceTypeMapper {

    public static String getName(CinemaPrice.PriceType type) {
        switch (type) {
            case REGULAR:
                return "Regular";
            case NORMAL_3D:
                return "Normal-3D";
            case HINDI:
                return "Hindi";
            case NORMAL_4DX:
                return "Normal-4DX";
            case SUPER_4DX:
                return "Super-4DX";
            case SWEET_BOX:
                return "Sweetbox";
            case GOLD_CLASS:
                return "Gold";
            case VELVET_CLASS:
                return "Velvet Normal";
            case VELVET_SUITE:
                return "Velvet Suite";
            case SPHERE_X:
                return "Sphere-X";
            case SPHERE_X_3D:
                return "Sphere-X 3D";
            case SATIN:
                return "Satin";
            case OTHER:
                return "Other";
            case SWEET_BOX_3D:
                return "Sweetbox-3D";
            case LOCAL:
                return "Local";
            case STARIUM:
                return "Starium";
            case STARIUM_3D:
                return "Starium-3D";
            default:
                return "Other";
        }
    }
}
