package com.felixsu.common.model.general;

import com.felixsu.common.model.CommonModel;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public class Acknowledge implements CommonModel {

    private String message;

    public Acknowledge() {
    }

    public Acknowledge(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
