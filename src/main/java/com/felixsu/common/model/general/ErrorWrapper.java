package com.felixsu.common.model.general;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public class ErrorWrapper {

    private Error error;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
