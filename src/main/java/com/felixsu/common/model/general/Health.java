package com.felixsu.common.model.general;

import com.felixsu.common.model.CommonModel;

import java.util.Date;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public class Health implements CommonModel {

    private String message;
    private String code;
    private Long timestamp;

    public Health() {
    }

    public Health(String message, String code) {
        this.message = message;
        this.code = code;
        this.timestamp = new Date().getTime();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
