package com.felixsu.common.model;

import java.io.Serializable;

/**
 * Created on 11/13/16.
 *
 * @author felixsoewito
 */
public interface EntityModel<ID extends Serializable> extends CommonModel {
    ID getId();
}
