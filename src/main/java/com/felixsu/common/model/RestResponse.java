package com.felixsu.common.model;

import com.felixsu.common.model.general.Error;

import java.util.List;

/**
 * Created on 11/13/16.
 *
 * @author felixsoewito
 */
public class RestResponse<T extends CommonModel> {

    private T response;
    private List<T> responses;
    private Error error;

    public RestResponse() {
    }

    public RestResponse(T response, List<T> responses, Error error) {
        this.response = response;
        this.responses = responses;
        this.error = error;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public List<T> getResponses() {
        return responses;
    }

    public void setResponses(List<T> responses) {
        this.responses = responses;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
