package com.felixsu.common.model.cinema;

import com.felixsu.common.model.EntityModel;

import java.io.Serializable;
import java.util.Date;

/**
 * Created on 11/22/16.
 *
 * @author felixsoewito
 */
public class CinemaDetail implements EntityModel<Long>, Serializable {

    private Long id;
    private String cinemaId;
    private CinemaDetail.DetailType type;
    private String value;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(String cinemaId) {
        this.cinemaId = cinemaId;
    }

    public DetailType getType() {
        return type;
    }

    public void setType(DetailType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public enum DetailType {

        ADDRESS(0),
        FEATURE(1),
        PHONE(2);

        int value;

        DetailType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}
