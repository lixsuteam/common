package com.felixsu.common.model.cinema;

import com.felixsu.common.model.EntityModel;
import com.felixsu.common.model.extra.CgvItem;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created on 11/9/16.
 *
 * @author felixsoewito
 */
public class Cinema implements
        EntityModel<String>,
        Serializable,
        CgvItem {

    public static final String CGV = "CGV";
    public static final String XXI = "XXI";

    private String id;
    private String cinemaName;
    private String city;
    private String cover;
    private String owner;
    private List<CinemaDetail> cinemaDetails;
    private List<CinemaPrice> cinemaPrices;

    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;

    public Cinema() {
    }

    public Cinema(String id) {
        this.id = id;
    }

    public Cinema(String id, String cinemaName, String city, String owner) {
        this(id);
        this.cinemaName = cinemaName;
        this.city = city;
        this.owner = owner;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public List<CinemaDetail> getCinemaDetails() {
        return cinemaDetails;
    }

    public void setCinemaDetails(List<CinemaDetail> cinemaDetails) {
        this.cinemaDetails = cinemaDetails;
    }

    public List<CinemaPrice> getCinemaPrices() {
        return cinemaPrices;
    }

    public void setCinemaPrices(List<CinemaPrice> cinemaPrices) {
        this.cinemaPrices = cinemaPrices;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cinema cinema = (Cinema) o;

        return id != null ? id.equals(cinema.id) : cinema.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
