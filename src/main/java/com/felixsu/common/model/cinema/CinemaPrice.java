package com.felixsu.common.model.cinema;

import com.felixsu.common.model.EntityModel;

import java.io.Serializable;
import java.util.Date;

/**
 * Created on 11/22/16.
 *
 * @author felixsoewito
 */
public class CinemaPrice implements EntityModel<Long>, Serializable {

    private Long id;
    private String cinemaId;
    private String holidayPrice;
    private String normalPrice;
    private String occasionalPrice;
    private CinemaPrice.PriceType type;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(String cinemaId) {
        this.cinemaId = cinemaId;
    }

    public String getHolidayPrice() {
        return holidayPrice;
    }

    public void setHolidayPrice(String holidayPrice) {
        this.holidayPrice = holidayPrice;
    }

    public String getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(String normalPrice) {
        this.normalPrice = normalPrice;
    }

    public String getOccasionalPrice() {
        return occasionalPrice;
    }

    public void setOccasionalPrice(String occasionalPrice) {
        this.occasionalPrice = occasionalPrice;
    }

    public PriceType getType() {
        return type;
    }

    public void setType(PriceType type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public enum PriceType {
        REGULAR(0),
        NORMAL_3D(1),
        HINDI(2),
        NORMAL_4DX(3),
        SUPER_4DX(4),
        SWEET_BOX(5),
        GOLD_CLASS(6),
        VELVET_CLASS(7),
        VELVET_SUITE(8),
        SPHERE_X(9),
        SPHERE_X_3D(10),
        SATIN(11),
        OTHER(12),
        SWEET_BOX_3D(13),
        LOCAL(14),
        STARIUM(15),
        STARIUM_3D(16);

        int value;

        PriceType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}
