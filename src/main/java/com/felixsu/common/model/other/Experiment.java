package com.felixsu.common.model.other;

import com.felixsu.common.model.CommonModel;

/**
 * Created on 1/4/17.
 *
 * @author felixsoewito
 */
public class Experiment implements CommonModel {

    private Long id;
    private String firstName;
    private String lastName;
    private String nickName;
    private Double weight;
    private Integer age;

    public Experiment() {
    }

    public Experiment(Long id, String firstName, Integer age) {
        this.id = id;
        this.firstName = firstName;
        this.age = age;
    }

    public Experiment(Long id, String firstName, String lastName, String nickName, Double weight, Integer age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.weight = weight;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
